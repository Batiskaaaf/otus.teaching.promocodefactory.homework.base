﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Сотрудники
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class EmployeesController
        : ControllerBase
    {
        private readonly IRepository<Employee> _employeeRepository;

        public EmployeesController(IRepository<Employee> employeeRepository)
        {
            _employeeRepository = employeeRepository;
        }
        
        /// <summary>
        /// Получить данные всех сотрудников
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<EmployeeShortResponse>> GetEmployeesAsync()
        {
            var employees = await _employeeRepository.GetAllAsync();

            var employeesModelList = employees.Select(x => 
                new EmployeeShortResponse()
                    {
                        Id = x.Id,
                        Email = x.Email,
                        FullName = x.FullName,
                    }).ToList();

            return employeesModelList;
        }
        
        /// <summary>
        /// Получить данные сотрудника по Id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<EmployeeResponse>> GetEmployeeByIdAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
                return NotFound();
            
            var employeeModel = new EmployeeResponse()
            {
                Id = employee.Id,
                Email = employee.Email,
                Roles = employee.Roles.Select(x => new RoleItemResponse()
                {
                    Name = x.Name,
                    Description = x.Description
                }).ToList(),
                FullName = employee.FullName,
                AppliedPromocodesCount = employee.AppliedPromocodesCount
            };

            return employeeModel;
        }

        ///<summary>
        ///Создать нового сотрудника
        ///</summary>
        [HttpPost]
        public async Task<ActionResult<Guid>> CreateEmployeeAsync(EmployeeCreateRequest request)
        {
            var newEmployee = new Employee(){
                FirstName = request.FirstName,
                LastName = request.LastName,
                Email = request.Email,
                Roles = new List<Role>(),
                AppliedPromocodesCount = 0
            };
            var guid = await _employeeRepository.CreateAsync(newEmployee);
            return Ok(guid);
        }

        ///<summary>
        ///Удалить сотрудника по Id
        ///</summary>
        [HttpDelete("{id:guid}")]
        public async Task<ActionResult> DeleteEmployeeByIdAsync(Guid id)
        {
            if(! await _employeeRepository.Exist(id))
                return NotFound();
            await _employeeRepository.DeleteAsync(id);
            return Ok();
        }

        ///<summary>
        ///Обновить информацию о сотруднике
        ///</summary>
        [HttpPut]
        public async Task<ActionResult> UpdateEmployeeAsync(EmployeeUpdateRequest request)
        {
            if(! await _employeeRepository.Exist(request.Id))
                return NotFound();
            var updatedEmployee = new Employee()
            {
                Id = request.Id,
                FirstName = request.FirstName,
                LastName = request.LastName,
                Email = request.Email,
                AppliedPromocodesCount = request.AppliedPromocodesCount
            };
            await _employeeRepository.UpdateAsync(updatedEmployee);
            return Ok();
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T>
        : IRepository<T>
        where T: BaseEntity
    {
        protected IEnumerable<T> Data { get; set; }

        public InMemoryRepository(IEnumerable<T> data)
        {
            Data = data;
        }
        
        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult(Data);
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }

        public Task<Guid> CreateAsync(T entity)
        {
            entity.Id = Guid.NewGuid();
            (Data as IList<T>).Add(entity);
            return Task.FromResult(entity.Id);
        }

        public Task DeleteAsync(Guid id)
        {
            var entityToDelete = Data.FirstOrDefault(e => e.Id.Equals(id));
            (Data as IList<T>).Remove(entityToDelete);
            return Task.CompletedTask;
        }

        public Task UpdateAsync(T entity)
        {
            var target = Data.FirstOrDefault(e => e.Id.Equals(entity.Id));
            foreach (var prop in entity.GetType().GetProperties().Where(p => p.SetMethod != null && p.Name != nameof(BaseEntity.Id)))
            {
                if(prop.PropertyType.IsGenericType 
                    &&  prop.PropertyType.GetGenericTypeDefinition().Equals(typeof(List<>)))
                {
                    if(prop.GetValue(entity) != null)
                        prop.SetValue(target,prop.GetValue(entity));
                }
                else 
                    prop.SetValue(target,prop.GetValue(entity));
            }
            return Task.CompletedTask;
        }

        public Task<bool> Exist(Guid id)
        {
            return Task.FromResult(Data.Any(e => e.Id.Equals(id)));
        }
    }
}